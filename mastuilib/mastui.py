import io
import logging
import subprocess
import sys

import urwid

from .api import ApiInstance
from .provider import *
from .widget import *
from .widget.dialog import *
from .widget.toot import *
from .config import Lang
from . import util

palette = [
        ('attachment', 'dark green,italics', ''),
        ('attachments header', 'dark red,bold', ''),
        ('button cancel', 'dark red,bold', ''),
        ('button cancel focus', 'dark red,bold', 'brown'),
        ('button ok', 'dark blue,bold', ''),
        ('button ok focus', 'dark blue,bold', 'light gray'),
        ('card', 'white,italics', ''),
        ('header', 'light magenta,bold', ''),
        ('poll option', 'dark green', ''),
        ('poll header', 'yellow,bold', ''),
        ('footer', 'dark red', ''),
        ('focus header', 'dark cyan,bold', ''),
        ('toot focus', 'white', ''),
        ('column header', 'white,bold', 'dark blue'),
        ('column header focus', 'dark red,bold', 'dark blue'),
        ('status_bar', 'white,bold', 'dark blue'),
        ('status_bar_error', 'white,bold', 'light red'),
        ('confirm_urgent', 'white,bold', 'dark red'),
        ('confirm_caution', 'white,bold', 'brown'),
        ('confirm_normal', 'white', 'dark blue'),
        ('post toot', 'white', 'dark blue'),
        ('post toot header', 'white,bold', 'dark cyan'),
        ('post toot footer', 'dark magenta,italics', ''),
        ('toot text', 'white,bold', ''),
        ('help menu', 'white', 'dark blue'),
        ('help menu focus', 'dark blue', 'white'),
        ('log_CRITICAL', 'dark red,bold', ''),
        ('log_ERROR', 'light red,bold', ''),
        ('log_WARNING', 'yellow,bold', ''),
        ('log_INFO', 'dark cyan', ''),
        ('log_DEBUG', 'dark green', '')
    ]

class Mastui(urwid.WidgetWrap):
    VERSION = '0.1b'
    def load_palette(self):
        for p in self.config.items('palette'):
            for i in range(0, len(palette)):
                if p[0] == palette[i][0]:
                    vals = p[1].split('|')
                    vals.insert(0, p[0])
                    palette[i] = tuple(vals)
                    break
        self.palette = palette

    def __init__(self, config):
        self.lang = Lang()
        self.config = config
        self.keymap = Mastui.KeyMap(self.config)
        self.init_logging()
        self.apis = []
        self.api = None
        for section_name in self.config.sections():
            if not section_name.startswith('account'):
                continue
            alias = section_name.split(' ')
            if len(alias) > 1:
                alias = alias[1]
            else:
                alias = user
            self.apis.append(ApiInstance(self, alias, self.config[section_name]))

        if len(self.apis) == 0:
            print('No accounts configured. Exiting.')
            sys.exit(0)

        util.configure(self.config)
        TootBase.configure(self.config)

        self.load_palette()
        self.api = None
        self.status_text = urwid.Text("Connecting APIs...", wrap='clip')
        self._status_normal = urwid.AttrMap(self.status_text, 'status_bar')
        self._status_error = urwid.AttrMap(self.status_text, 'status_bar_error')
        self.status_delay = 0
        # Dummy column to workaround focus issues
        self.cols = [Column(self, None, ContentProvider())]
        self.top = urwid.Columns(self.cols, dividechars=1, min_width=40)
        #self.top.focus_position = 0
        self.zoom_idx = None

        self.frame = urwid.Pile([self.top, (1, urwid.Filler(self._status_normal))])
        super().__init__(self.frame)

    class LogBuffer(logging.StreamHandler):
        def __init__(self, capacity):
            self.capacity = capacity
            self.buf = []
            super().__init__()

        def emit(self, record):
            text = self.format(record)
            self.buf.insert(0, (record.levelname, text))
            if len(self.buf) > self.capacity:
                self.buf.pop()
        def lines(self):
            return self.buf

    def init_logging(self):
        loglevel = self.config.get('logging', 'level', fallback='WARNING').upper()
        logfmt = self.config.get('logging', 'format', fallback='%(asctime)s [%(levelname)s]: %(message)s')
        logdatefmt = self.config.get('logging', 'dateformat', fallback='%Y-%m-%d %H:%M:%S')
        level = getattr(logging, loglevel, None)
        warn_about_level = False
        if not isinstance(level, int):
            level = logging.WARNING

        handlers = []
        # Setup the file log handler
        filename = self.config.get('logging', 'file', fallback=None)
        if filename:
            handlers.append(logging.FileHandler(filename))

        # Setup the memory log handler
        self.logbuf = self.LogBuffer(self.config.getint('logging', 'buffersize', fallback=100))
        handlers.append(self.logbuf)

        logging.basicConfig(format=logfmt, level=level, handlers=handlers)

        logging.info('Logging initialized, log file: {}'.format(filename if filename else "<none>"))
        if warn_about_level:
            logging.warning('Invalid value specified for mastui.loglevel: "{}", defaulted to WARNING'.format(loglevel))

    def connect_apis(self):
        for api in self.apis:
            api.log_in()
        self.api = self.apis[0]
        self.add_or_focus_timeline('Local', 'local')
        self._close_column(0) # Close the dummy column

    def run(self):
        self.loop = urwid.MainLoop(self, palette, pop_ups=True)
        self.loop.set_alarm_in(1, self.tick, None)
        self.loop.run()

    def close(self):
        for api in self.apis:
            api.close()
        raise urwid.ExitMainLoop()

    def tick(self, loop, data):
        if not self.api: self.connect_apis()
        for col in self.cols: col.provider.tick()
        if self.status_delay > 0: self.status_delay -= 1
        self.update_status()
        loop.set_alarm_in(1, self.tick, loop)

    def _add_column(self, c, idx=None):
        if idx is not None:
            self.cols.insert(idx+1, c)
            self.top.contents.insert(idx+1, (c, self.top.options()))
            self.top.focus_position = idx+1
        else:
            self.cols.append(c)
            self.top.contents.append((c, self.top.options()))
            self.top.focus_position = len(self.top.contents) - 1

    def _close_column(self, idx):
        del self.cols[idx]
        del self.top.contents[idx]

    def add_or_focus_timeline(self, title, timeline):
        for i in range(0, len(self.top.contents)):
            if self.top.contents[i][0].provider.match(timeline, api=self.api):
                self.top.focus_position = i
                return
        self._add_column(Column(self, self.api, TimelineProvider(self.api, timeline=timeline)))

    def open_raw_column(self, obj):
        self._add_column(Column(self, self.api, RawProvider(self.api, obj=obj)), idx=self.top.focus_position)

    def open_context_column(self, toot_id):
        toot = self.api.api.status(toot_id)
        self._add_column(Column(self, self.api, ContextProvider(self.api, toot=toot)), idx=self.top.focus_position)

    def open_column(self, prov):
        self._add_column(Column(self, self.api, prov), idx=self.top.focus_position)

    def open_notifications_column(self):
        self._add_column(Column(self, self.api, NotificationsProvider(self.api)))

    def open_bookmarks_column(self):
        self._add_column(Column(self, self.api, BookmarksProvider(self.api)))

    def _open_hashtag_column(self, arg):
        if arg:
            if arg[0] == '#': arg = arg[1:]
            self._add_column(Column(self, self.api, TimelineProvider(self.api, timeline='hashtag', args=arg)))

    def open_hashtag_column(self):
        SearchDialog(self, title='Hashtag Search', confirm_action=self._open_hashtag_column).show()

    def open_log_column(self):
        self._add_column(Column(self, self.api, LogProvider(self.logbuf)))

    def open_account_column(self, toot):
        self._add_column(AccountColumn(self, self.api, AccountProvider(self.api, toot['account']['id'])))

    def toggle_zoom(self):
        if self.zoom_idx is None:
            self.zoom_idx = self.top.focus_position
            focus_col = self.top.contents[self.zoom_idx]
            self.top.contents.clear()
            self.top.contents.append(focus_col)
        else:
            self.zoom_idx = None
            self.top.contents.clear()
            self.top.contents = [(c, self.top.options()) for c in self.cols]

    def _update_status_style(self, error=False):
        if error: self.frame.contents[1][0].original_widget = self._status_error
        else: self.frame.contents[1][0].original_widget = self._status_normal

    def update_status(self, text=None, is_error=False):
        if text is None and self.status_delay == 0:
            self._update_status_style()

            from datetime import datetime
            self.status_text.set_text("[{}] Rate: {}/{}, Updated: {}, Reset: {}".format(
                self.api.alias,
                self.api().ratelimit_remaining,
                self.api().ratelimit_limit,
                util.format_date(datetime.fromtimestamp(self.api().ratelimit_lastcall)),
                util.format_date(datetime.fromtimestamp(self.api().ratelimit_reset))))
        elif text is not None:
            if is_error: self._update_status_style(True)
            self.status_text.set_text(text)
            self.status_delay = 3

    def open_links(self, link=None, links=[]):
        types = {}
        if link: types[link[1]] = link[0]
        elif links:
            for url,typ in links:
                types[typ] = types.get(typ, '') + ' {}'.format(url)
        for filetype,url in types.items():
            cmd = self.config.get('viewer', filetype, fallback=None)
            if not cmd:
                cmd = self.config.get('viewer', 'default', fallback=None)
            if cmd:
                logging.debug('Opening link "{}" with type "{}" using command {}'.format(url, filetype, cmd))
                args = cmd.format(url).split(' ')
                try:
                    proc = subprocess.Popen(args,
                            close_fds=True,
                            #stdout=subprocess.DEVNULL,
                            stderr=subprocess.DEVNULL)
                    proc.wait()
                except Exception as e:
                    self.update_status('Failed displaying link: {}'.format(str(e)), is_error=True)

                self.loop.screen.clear()
            else:
                self.update_status('No viewer for type "{}" and no default configured.'.format(filetype), is_error=True)

    class KeyMap:
        class KeyBind:
            def __init__(self, name, keys, helptext=None):
                self.name = name
                self.keys = keys
                self.helptext = helptext if helptext else self.name
            def match(self, key):
                if key in self.keys: return True
                else: return False

        def add_key(self, name, default, helptext):
            mapping = self.cfg.get('keymap', name, fallback=None)
            if mapping: mapping = [k.strip() for k in mapping.split()]
            else: mapping = default
            self.keys[name] = self.KeyBind(name, mapping, helptext)

        def __init__(self, cfg):
            self.cfg = cfg
            self.keys = {}
            self.add_key('bookmark_toot', ['K'], 'Bookmark selected toot')
            self.add_key('bookmarks', ['ctrl k'], 'Open Bookmarks column')
            self.add_key('boost_toot', ['b'], 'Boost selected toot')
            self.add_key('block_account', ['B'], 'Block account of selected toot')
            self.add_key('close_col', ['q'], 'Close current column')
            self.add_key('delete_toot', ['d'], 'Deleted selected own toot')
            self.add_key('fav_toot', ['f'], 'Favorite selected toot')
            self.add_key('federated', ['ctrl f'], 'Open Federated timeline')
            self.add_key('focus_left', ['left', 'h'], 'Focus column left')
            self.add_key('follow_account', ['F'], 'Follow account of selected toot')
            self.add_key('home', ['backspace'], 'Open Home timeline') # ctrl h
            self.add_key('focus_right', ['right', 'l'], 'Focus column right')
            self.add_key('local', ['ctrl l'], 'Open Local timeline')
            self.add_key('log', ['ctrl g'], 'Open Log viewer')
            self.add_key('move_left', ['H'], 'Move column left')
            self.add_key('move_right', ['L'], 'Move column right')
            self.add_key('mute_account', ['M'], 'Mute account of selected toot')
            self.add_key('new_toot', ['T'], 'Open new toot dialog')
            self.add_key('next_account', ['ctrl right'], 'Switch to next account')
            self.add_key('notifications', ['ctrl n'], 'Open Notifications timeline')
            self.add_key('open_account', ['ctrl a'], 'Open Account column')
            self.add_key('open_context', ['enter'], 'Open Context column')
            self.add_key('open_raw', ['ctrl r'], 'Open Raw column')
            self.add_key('prev_account', ['ctrl left'], 'Switch to previous account')
            self.add_key('reload', ['R'], 'Reload column')
            self.add_key('reply', ['r'], 'Reply to selected toot')
            self.add_key('select_card', ['c'], 'Activate card link')
            self.add_key('select_1', ['1'], 'Activate selectable item 1 (media/poll)')
            self.add_key('select_2', ['2'], 'Activate selectable item 2 (media/poll)')
            self.add_key('select_3', ['3'], 'Activate selectable item 3 (media/poll)')
            self.add_key('select_4', ['4'], 'Activate selectable item 4 (media/poll)')
            self.add_key('select_*', ['*'], 'Activate all selectable items (media)')
            self.add_key('toggle_collapse', [' '], 'Toggle media collapse state')
            self.add_key('quit', ['Q'], 'Quit mastui')
            self.add_key('update', ['u'], 'Fetch updates for column')
            self.add_key('zoom', ['z'], 'Toggle zoom current column')

        def map(self, key):
            for name,bind in self.keys.items():
                if key in bind.keys: return name
            return None

    def keypress(self, size, key):
        if self.api is None: return False
        if len(self.top.contents) > 0:
            if self.top.focus.keypress(size, key):
                return True
        cmd = self.keymap.map(key)
        if cmd == 'quit':
            self.close()
        elif cmd == 'focus_left':
            if self.top.focus_position > 0:
                self.top.focus_position -= 1
        elif cmd == 'move_left':
            idx = self.top.focus_position
            if idx > 0:
                col = self.top.contents.pop(idx)
                self.top.contents.insert(idx-1, col)
                self.top.focus_position = idx-1
        elif cmd == 'focus_right':
            if self.top.focus_position < len(self.top.contents) - 1:
                self.top.focus_position += 1
        elif cmd == 'move_right':
            idx = self.top.focus_position
            if idx < len(self.top.contents)-1:
                col = self.top.contents.pop(idx)
                self.top.contents.insert(idx+1, col)
                self.top.focus_position = idx+1
        elif cmd == 'zoom':
            self.toggle_zoom()
        elif cmd == 'close_col':
            if len(self.top.contents) > 1:
                self._close_column(self.top.focus_position)
            elif self.zoom_idx is not None:
                self.toggle_zoom()
        elif cmd == 'federated':
            self.add_or_focus_timeline('Federated', 'public')
        elif cmd == 'local':
            self.add_or_focus_timeline('Local', 'local')
        elif cmd == 'log':
            self.open_log_column()
        elif key == 'ctrl t':
            self.open_hashtag_column()
            pass
        elif cmd == 'new_toot':
            TootDialog(self.api).show()
        elif cmd == 'notifications':
            self.open_notifications_column()
        elif cmd == 'bookmarks':
            self.open_bookmarks_column()
        elif cmd == 'next_account':
            curidx = self.apis.index(self.api)
            if curidx > 0:
                self.api = self.apis[curidx-1]
        elif cmd == 'prev_account':
            curidx = self.apis.index(self.api)
            if curidx < len(self.apis) - 1:
                self.api = self.apis[curidx+1]
        elif cmd == 'home':
            self.add_or_focus_timeline('Home', 'home')
        elif key == '?':
            HelpMenuDialog(self).show()
        else:
            self.update_status('Unhandled key: {}'.format(key))
            return False
        self.update_status()
        return True

