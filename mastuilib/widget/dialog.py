import urwid
from .. import util

class Dialog(urwid.WidgetWrap):
    def __init__(self, app, w):
        self.app = app
        super().__init__(w)
    def close(self):
        self.app.loop.widget = self.restore_w
    def show(self):
        self.restore_w = self.app.loop.widget
        self.app.loop.widget = urwid.Overlay(self, self.app.loop.widget, align=("relative", 50), valign=("relative", 50), width=self.width(), height=self.height()) #("relative", 10))

    def width(self):
        return 10
    def height(self):
        return 10

class ConfirmDialog(Dialog):
    signals = ['close']
    def __init__(self, app, text, yes_cb, yes_arg=None, no_cb=None, no_arg=None, palette='confirm_normal'):
        self.yes_cb = yes_cb
        self.yes_arg = yes_arg
        self.no_cb = no_cb
        self.no_arg = no_arg
        self.min_width = len(text)

        yes_btn = urwid.Button("(Y)es")
        no_btn = urwid.Button("(N)o")
        #urwid.connect_signal(close_button, 'click', lambda button:self._emit("close"))
        pile = urwid.Pile([urwid.Text(text), yes_btn, no_btn])
        fill = urwid.Filler(pile)
        super().__init__(app, urwid.AttrMap(fill, palette))

    def width(self):
        return self.min_width + 2
    def height(self):
        return 3

    def keypress(self, size, key):
        if key in ['y', 'Y']: self.yes_cb(self.yes_arg)
        elif key in ['n', 'N', 'esc', 'q', 'Q']:
            if self.no_cb: self.no_cb(self.no_arg)
            else: self.app.update_status('Aborted.')
        else: return True
        self.close()
        return True

class TootDialog(Dialog):
    def __init__(self, api, reply_to=None):
        self.api = api
        self.reply_to = reply_to
        self.poll_opts = None
        self.poll = None
        self.visibility = None
        self.vis_list = [key for key,_ in util.VIS_MAP.items()]
        self.vis_list.insert(0, None)

        items = []
        if self.reply_to:
            header_text = urwid.AttrMap(urwid.Text(TootBase.clean(self.reply_to['content'])), 'toot text')
            header = urwid.LineBox(header_text, title=util.Fmt('Reply to @{account.acct}:', self.reply_to), title_align='left')
        else:
            header = urwid.AttrMap(urwid.Text('New Toot'), 'post toot header')

        self.text = urwid.AttrMap(header, 'post toot header')
        self.text._selectable = False
        items.append(self.text)

        self.inbox = urwid.Edit(multiline=True, wrap='space', allow_tab=False)
        inbox_f = urwid.AttrMap(urwid.Filler(self.inbox, valign='top'), 'post toot')
        self.inbox_lb = urwid.AttrMap(urwid.LineBox(inbox_f), 'post toot header')
        items.append((10, self.inbox_lb))
        self.footer = urwid.Text('[{}] Characters remaining: {}'.format(
            util.map_visibility(self.visibility), self.api.limits.toot_chars), align='right') # TODO: Move to header
        items.append(urwid.AttrMap(self.footer, 'post toot footer'))
        ok_btn = urwid.AttrMap(urwid.Button("OK"), 'button ok', 'button ok focus')
        urwid.connect_signal(ok_btn.original_widget, 'click', self._on_ok, None)
        items.append(ok_btn)
        cancel_btn = urwid.AttrMap(urwid.Button("Cancel"), 'button cancel', 'button cancel focus')
        urwid.connect_signal(cancel_btn.original_widget, 'click', self._on_cancel, None)
        items.append(cancel_btn)

        self.pile = urwid.Pile(items)
        fill = urwid.Filler(self.pile)
        super().__init__(api.app, fill)

    def get_char_count(self):
        cnt = len(self.inbox.edit_text)
        if self.poll: cnt += self.poll.get_char_count()
        return cnt

    def add_poll_option(self):
        if not self.poll:
            self.poll = PollWidget(self.api)
            for i in range(0, len(self.pile.contents)):
                if self.pile.contents[i][0] == self.inbox_lb:
                    self.pile.contents.insert(i+1, (self.poll, self.pile.options()))
                    break
        else:
            self.poll.add_poll_option()

    def toggle_visibility(self):
        idx = self.vis_list.index(self.visibility)
        if idx < len(self.vis_list) - 1: self.visibility = self.vis_list[idx+1]
        else: self.visibility = self.vis_list[0]

    def _on_ok(self, arg):
        if not self.inbox.edit_text == '':
            poll = None
            if self.poll: poll = self.poll.make_poll()
            reply_to_id = self.reply_to['id'] if self.reply_to else None
            self.api.post_toot(self.inbox.edit_text, reply_to_toot=self.reply_to, in_reply_to_id=reply_to_id, poll=poll)
        self.close()
    def _on_cancel(self, arg):
        self.close()

    def width(self):
        return 80
    def height(self):
        return 25

    def keypress(self, size, key):
        if key == 'tab': key = 'down'
        elif key == 'shift tab': key = 'up'

        sz = (self.width(),)
        if key is not None:
            key = self.pile.keypress(sz, key)
        if key is not None:
            if key == 'esc': self.close()
            elif key == 'ctrl enter': self._on_ok(None)
            elif key == 'ctrl p': self.add_poll_option()
            elif key == 'ctrl v': self.toggle_visibility()
        self.footer.set_text('[{}] Characters remaining: {}'.format(
            util.map_visibility(self.visibility), self.api.limits.toot_chars - self.get_char_count()))
        return None

class PollWidget(urwid.WidgetWrap):
    def __init__(self, api):
        self.api = api
        new_opt = urwid.AttrMap(urwid.Edit(caption='Option: '), 'poll option')
        self.listwalker = urwid.SimpleListWalker([new_opt])
        self.listbox = urwid.ListBox(self.listwalker)

        self.expiry = urwid.Edit(caption='Expiry (#d#h#m): ', edit_text='1d')
        self.allow_multiple = urwid.CheckBox('Allow multiple: ')
        self.hide_totals = urwid.CheckBox('Hide totals: ')

        # TODO: Get rid of BoxAdapter?
        pile = urwid.Pile([urwid.BoxAdapter(self.listbox, self.api.limits.poll_options), self.expiry, self.allow_multiple, self.hide_totals])
        box = urwid.LineBox(pile, title='Poll')
        urwid.WidgetWrap.__init__(self, box)

    def add_poll_option(self):
        if (len(self.listwalker.contents) < self.api.limits.poll_options):
            new_opt = urwid.AttrMap(urwid.Edit(caption='Option: '), 'poll option')
            self.listwalker.extend([new_opt])

    def get_char_count(self):
        cnt = 0
        for opt in self.listwalker:
            cnt += len(opt.original_widget.edit_text)
        return cnt
    
    def make_poll(self):
        opts = [opt.original_widget.edit_text for opt in self.listwalker]
        try:
            expiry = parse_duration(self.expiry.edit_text)
        except:
            expiry = 24 * 60 * 60 # one day, in seconds
        return self.api.make_poll(opts, expiry, multiple=self.allow_multiple.state, hide_totals=self.hide_totals.state)

class SearchDialog(Dialog):
    def __init__(self, app, title, confirm_action):
        self.text = urwid.AttrMap(urwid.Edit(caption='Search: '), 'help menu')
        self.confirm_action = confirm_action

        ok_btn = urwid.AttrMap(urwid.Button("OK", on_press=self.on_confirm), 'button ok', 'button ok focus')
        cancel_btn = urwid.AttrMap(urwid.Button("Cancel", on_press=self.on_close), 'button cancel', 'button cancel focus')

        self.content = urwid.Pile([self.text, ok_btn, cancel_btn])
        fill = urwid.Filler(self.content)
        self._width = 40
        self._height = 3
        super().__init__(app, fill)

    def width(self):
        return self._width
    def height(self):
        return self._height

    def on_close(self, args):
        self.close()

    def on_confirm(self, args):
        self.close()
        self.confirm_action(self.text.original_widget.edit_text)

class HelpListDialog(Dialog):
    def __init__(self, app, title, values):
        items = []
        self._width = 0
        self._height = 2
        for text in values:
            self._height += 1
            if len(text) > self._width: self._width = len(text)
            items.append(urwid.Text(text))

        self._width += 2
        self.content = urwid.ListBox(items)
        helpbox = urwid.LineBox(self.content, title=title)
        #self.listbox = urwid.ListBox(items)
        super().__init__(app, urwid.AttrMap(helpbox, 'help menu'))

    def width(self):
        return self._width
    def height(self):
        return self._height
    def keypress(self, size, key):
        self.close()
        return True

class HelpMenuDialog(Dialog):
    def __init__(self, app):
        keys_btn = urwid.AttrMap(urwid.Button("(K)eys", on_press=self._open_keys), 'help menu', 'help menu focus')
        styles_btn = urwid.AttrMap(urwid.Button("(S)tyles", on_press=self._open_styles), 'help menu', 'help menu focus')
        about_btn = urwid.AttrMap(urwid.Button("(A)bout", on_press=self._open_about), 'help menu', 'help menu focus')
        self.content = urwid.ListBox([keys_btn, styles_btn, about_btn])
        self.content.set_focus(0)
        self._width = 20
        self._height = 2 + len(self.content.body)

        helpbox = urwid.LineBox(self.content, title='Help')
        #self.listbox = urwid.ListBox(items)
        super().__init__(app, urwid.AttrMap(helpbox, 'help menu'))

    def width(self):
        return self._width
    def height(self):
        return self._height
    def _open_keys(self, arg=None):
        self.close()
        HelpListDialog(self.app, 'Keybindings',
                [' {}: ({}) - {} '.format(bind.keys, bind.name, bind.helptext)
                    for name,bind in self.app.keymap.keys.items()]).show()
    def _open_styles(self, arg=None):
        self.close()
        HelpListDialog(self.app, 'Styles',
                [' {}: fg=[{}] bg=[{}] '.format(p[0], p[1], p[2])
                    for p in self.app.palette]).show()
    def _open_about(self, arg=None):
        self.close()
        text = [
                'mastui - A terminal mastodon client',
                '(c) 2022 @mh',
                'https://gitlab.com/mth/mastui',
                '---',
                'Depends:',
                'Mastodon.py: https://github.com/halcy/Mastodon.py',
                'Urwid: http://urwid.org/',
                'html-text: https://github.com/TeamHG-Memex/html-text'
                ]
        HelpListDialog(self.app, 'About', text).show()
    def keypress(self, size, key):
        if key in ['up', 'down']:
            return self.content.keypress(size, key)
        elif key.upper() == 'K':
            self._open_keys()
        elif key.upper() == 'S':
            self._open_styles()
        elif key.upper() == 'A':
            self._open_about()
        elif key == 'enter':
            foc = self.content.get_focus()
            foc[0].original_widget.mouse_event(size, 'mouse press', 1, 4, 0, True)
        elif key in ['q', 'Q', 'esc']: self.close()
        return False

