import html_text
import urwid

from .. import util

class TootBase(urwid.WidgetWrap):
    def configure(config):
        TootBase.cfg = config
    def Match(obj):
        return False
    def clean(content):
        # Strip out span tags, they result in spaces in output
        import re
        content = re.sub(r"<[/]*span\b[^>]*>", '', content)
        tree = html_text.parse_html(content)
        cleaned_tree = html_text.cleaner.clean_html(tree)
        text = html_text.etree_to_text(cleaned_tree)
        if TootBase.cfg.get('mastui', 'demojize', fallback=True):
            import emoji
            text = emoji.demojize(text)
        return text

    def format_card(self, toot):
        card = toot.get('card')
        if not card: return None
        items = []
        items.append(urwid.Text('Card: {}'.format(card.get('title', 'No title'))))
        desc = card.get('description', '')
        if desc: items.append(urwid.Text(desc))
        author = card.get('author', None)
        if author: items.append(urwid.Text('Author: {}'.format(author)))
        prov_name = card.get('provider_name', None)
        if prov_name: items.append(urwid.Text('Provider: {}'.format(prov_name)))
        return urwid.LineBox(urwid.Pile(items), tline=' ', bline=' ', rline='', trcorner='', brcorner='')

    def format_attachments(self, toot, collapsed=False):
        self.media_collapsed = collapsed
        attachments = toot.get('media_attachments')
        if not attachments: return None
        media = []

        media.append(urwid.AttrMap(urwid.Text('{} Attachments ({})'.format(
            '+' if collapsed else '-', len(attachments))), 'attachments header'))
        if not collapsed:
            for i in range(0, len(attachments)):
                attach = attachments[i]
                media.append(urwid.AttrMap(urwid.Text(util.Fmt('{O_index}) [{type}]: {O_shorturl}',
                    attach, O_index=i+1, O_shorturl=util.shorten_url(attach.url)), wrap='clip'), 'attachment'))

        return urwid.Pile([(1, urwid.Filler(m)) for m in media])

    def format_poll(self, toot, collapsed=False):
        self.media_collapsed = collapsed
        poll = toot.get('poll')
        if not poll: return None
        items = []

        items.append(urwid.AttrMap(urwid.Text('{} Poll:'.format('+' if collapsed else '-')), 'poll header'))
        if not collapsed:
            own_votes = poll['own_votes']
            for i in range(0, len(poll['options'])):
                opt = poll['options'][i]
                voted = ''
                if i in own_votes:
                    voted = TootBase.cfg.get('mastui', 'poll_selected_option', fallback='*')
                vote_count = '?'
                if opt['votes_count'] != None: vote_count = opt['votes_count']
                items.append(urwid.AttrMap(urwid.Text(util.Fmt(
                    '{O_index}) {title}: {O_votes_count}{O_voted}', opt, O_index=i+1, O_votes_count=vote_count, O_voted=voted)), 'poll option'))
        return urwid.Pile(items)

    def format_footer(self, toot):
        if toot.get('type', '') == 'follow': return None
        return util.Fmt('R({replies_count}) F({favourites_count}{favourited}) B({reblogs_count}{reblogged}) [{visibility}]', toot)

    def create_widget(self, header, footer=None, card=None, content=None, media=None, poll=None):
        widgets = [urwid.AttrMap(header, 'header', 'focus header')]
        if content: widgets.append(content)
        if card: widgets.append(urwid.AttrMap(card, 'card'))
        if media:
            widgets.append(urwid.AttrMap(media, 'media'))
            self._media_idx = len(widgets)-1
            self._collapse_cb = self.format_attachments
        elif poll:
            widgets.append(poll)
            self._media_idx = len(widgets)-1
            self._collapse_cb = self.format_poll
        else: self._media_idx = -1
        if footer: widgets.append(urwid.AttrMap(footer, 'footer'))
        self._pile = urwid.Pile(widgets)
        return self._pile

    def toggle_collapse(self):
        if self._media_idx > 0:
            self._pile.contents[self._media_idx] = (
                    self._collapse_cb(self.toot, not self.media_collapsed),
                    self._pile.options(height_type='pack', height_amount=None))

    def get_toot(self):
        return self.toot

    def create(prov, toot):
        if BoostedToot.Match(prov, toot): return BoostedToot(toot)
        elif Notification.Match(prov, toot): return Notification(toot)
        else: return Toot(toot)

    def redraw(self):
        self.footer.set_text(self.format_footer(self.get_toot()))

class Toot(TootBase):
    def __init__(self, toot, header=None):
        self.toot = toot
        if header: self.header = header
        else: self.header = urwid.Text(util.Fmt('[{created_at}] (@{account.acct}) {account.display_name}', toot))

        text = None
        if self.toot and 'content' in self.toot:
            text = TootBase.clean(self.toot['content'])
        if text: self.content = urwid.Text(text)
        else: self.content = None
        self.card = self.format_card(self.toot)
        self.media = self.format_attachments(self.toot, TootBase.cfg.getboolean('mastui', 'collapse_attachments', fallback=False))
        self.poll = self.format_poll(self.toot, TootBase.cfg.getboolean('mastui', 'collapse_polls', fallback=False))
        footer_text = self.format_footer(self.toot)
        if footer_text: self.footer = urwid.Text(footer_text)
        else: self.footer = None

        self.display = self.create_widget(self.header, footer=self.footer, content=self.content, card=self.card, media=self.media, poll=self.poll)
        urwid.WidgetWrap.__init__(self, self.display)

class BoostedToot(Toot):
    def Match(prov, obj):
        return obj.get('reblog') != None
    def __init__(self, toot):
        self.real_toot = toot
        self.toot = toot['reblog']

        self.header = urwid.Text(util.Fmt(
            '[{reblog.created_at}] (@{reblog.account.acct}) {reblog.account.display_name} (boosted by {account.display_name})', toot))
        super().__init__(self.toot, header=self.header)

class Notification(Toot):
    TYPE_MAP = {
            'mention': 'mentioned you',
            'reblog': 'boosted your toot',
            'favourite': 'favorited your toot',
            'follow': 'followed you',
            'poll': 'poll result',
            'follow_request': 'requested to follow you'
            }
    def Match(prov, obj):
        t = obj.get('type')
        if not t: return False
        return Notification.TYPE_MAP.get(t) != None
    def map_type(self, action):
        if action in self.TYPE_MAP: return self.TYPE_MAP[action]
        else: return action

    def __init__(self, notification):
        self.notification = notification
        self.header = urwid.Text(util.Fmt('[{created_at}] (@{account.acct}) {account.display_name} {O_type}',
            notification, O_type=self.map_type(notification['type'])))
        self.toot = notification.get('status')
        if not self.toot: self.toot = notification
        super().__init__(self.toot, header=self.header)

class Column(urwid.WidgetWrap):
    def __init__(self, app, api, provider):
        self.app = app
        self.api = api
        self.title = provider.get_title()
        self.provider = provider

        self.head = urwid.AttrMap(urwid.Text(self.title), 'column header') #, 'column header focus')
        # Set _selectable?

        self.listbox = urwid.ListBox(self.provider.content)
        self.pile = urwid.Pile(self._get_items(), focus_item=self.listbox)
        super().__init__(self.pile)

    def _get_items(self):
        return [(1, urwid.Filler(self.head)), self.listbox]

    def keypress(self, size, key):
        new_status = None
        if self.listbox.focus:
            if self.provider.keypress(size, key):
                return True
        if key in ['up', 'k']:
            focus_widget, idx = self.listbox.get_focus()
            if idx is not None and idx > 0: self.listbox.set_focus(idx-1)
        elif key in ['down', 'j']:
            focus_widget, idx = self.listbox.get_focus()
            if idx is not None and idx < len(self.provider.content) - 1:
                self.listbox.set_focus(idx+1)
            else:
                if self.provider.more():
                    self.listbox.set_focus(idx+1)
        elif key in ['page down', 'page up', 'home', 'end']:
            self.listbox.keypress(size, key)
        else:
            return False
        return True

class AccountColumn(Column):
    def __init__(self, app, api, provider):
        super().__init__(app, api, provider)
    def _get_items(self):
        self.account_detail = urwid.Pile([
            (1, urwid.Filler(urwid.Text(util.Fmt('Account #{id}: [@{username}] {display_name} Bot: {bot}', self.provider.account)))),
            (1, urwid.Filler(urwid.Text(util.Fmt('Toots: {statuses_count} Followers: {followers_count} Following: {following_count}', self.provider.account))))
            ])
        return [(1, urwid.Filler(self.head)), self.account_detail, self.listbox]

