import logging
from mastodon import Mastodon
import queue
import threading


class ApiInstance:
    class ApiThread(threading.Thread):
        def __init__(self, alias, queue):
            self.alias = alias
            self.queue = queue
            super().__init__()
        def run(self):
            import logging
            logging.info('Work thread for {} is running.'.format(self.alias))
            while True:
                task = None
                try: task = self.queue.get(timeout=1)
                except queue.Empty: continue
                # Sentinel to exit
                if task is None: break
                try:
                    task()
                except Exception as e:
                    logging.warning('Exception in work thread: {}'.format(str(e)))
            logging.info('Work thread for {} is exiting.'.format(self.alias))

    class Limits:
        def __init__(self, section):
            self.toot_chars = section.get('toot_chars_limit', fallback=500)
            self.poll_options = section.get('poll_options_limit', fallback=4)

    def __init__(self, app, alias, section):
        self.alias = alias
        self.app = app

        self._user_creds = section.get('user_credentials_file')
        self._user = section.get('login_account')
        self._password = section.get('login_password', fallback=None)

        self._work = queue.Queue()
        self._thread = self.ApiThread(self.alias, self._work)

        url = section.get('base_url')
        client_creds = section.get('client_credentials_file')
        from os.path import exists
        if not exists(client_creds):
            Mastodon.create_app(
                 'mastui',
                 api_base_url = url,
                 to_file = client_creds
            )
        mast = Mastodon(
            client_id = client_creds,
            api_base_url = url
        )

        self.api = mast
        self.limits = self.Limits(section)

        self.account = self.api.account
        self.bookmarks = self.api.bookmarks
        self.make_poll = self.api.make_poll
        self.notifications = self.api.notifications
        self.timeline = self.api.timeline
        self.timeline_hashtag = self.api.timeline_hashtag
        self._thread.start()

    def close(self):
        self._work.put(None)
        self._thread.join()

    def do_async(self, task):
        self._work.put(task)

    def cfg(self):
        return self.app.config

    def __call__(self):
        return self.api

    def log_in(self):
        self.api.log_in(self._user, self._password, to_file=self._user_creds)

    def post_toot(self, text, reply_to_toot=None, **kwargs):
        if reply_to_toot is not None: self.api.status_reply(reply_to_toot, text, **kwargs)
        else: self.api.status_post(text, **kwargs)

    def delete_toot(self, toot):
        self.api.status_delete(toot['id'])
        self.app.update_status(self.app.lang.DELETED[1].format_map(toot))

    def follow_user(self, account):
        self.api.account_follow(account['id'])
        self.app.update_status(self.app.lang.FOLLOWED[1].format_map(account))

    def block_user(self, account):
        self.api.account_block(account['id'])
        self.app.update_status(self.app.lang.BLOCKED[1].format_map(account))

    def mute_user(self, account):
        self.api.account_mute(account['id'])
        self.app.update_status(self.app.lang.MUTED[1].format_map(account))

    def toggle_favorite(self, toot, onoff):
        if onoff: self.api.status_favourite(toot['id'])
        else: self.api.status_unfavourite(toot['id'])
        self.app.update_status(self.app.lang.FAVORITED[int(onoff)].format_map(toot))

    def toggle_boost(self, toot, onoff):
        if onoff: self.api.status_reblog(toot['id'])
        else: self.api.status_unreblog(toot['id'])
        self.app.update_status(self.app.lang.BOOSTED[int(onoff)].format_map(toot))

    def toggle_bookmark(self, toot, onoff):
        if onoff: self.api.status_bookmark(toot['id'])
        else: self.api.status_unbookmark(toot['id'])
        self.app.update_status(self.app.lang.BOOKMARKED[int(onoff)].format_map(toot))

