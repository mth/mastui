from datetime import datetime
from dateutil import tz

DATE_FORMAT = '%x %X'
ADJUST_TZ = True
DEMOJIZE = True
_tzutc = tz.tzutc()
_tzloc = tz.tzlocal()

VIS_MAP = {
        'public': 'pub',
        'unlisted': 'unl',
        'private': 'pvt',
        'direct': 'dir'
        }

def configure(config):
    DATE_FORMAT = config.get('mastui', 'dateformat', fallback='%x %X')
    ADJUST_TZ = config.getboolean('mastui', 'use_local_time', fallback=True)
    DEMOJIZE = config.get('mastui', 'demojize', fallback=True)

def map_visibility(vis):
    if vis is None: return 'def'
    else: return VIS_MAP.get(vis, '???')

def format_date(dt):
    #utc = dt.replace(tzinfo=_tzutc)
    if ADJUST_TZ: dt = dt.astimezone(_tzloc)
    return dt.strftime(DATE_FORMAT)

def parse_duration(s):
    days = 0
    hours = 0
    minutes = 0
    val = s
    for i in range(0, len(s)):
        if not days and s[i] == 'd':
            days = int(val[0:val.find('d')])
            val = s[i+1:]
        elif not hours and s[i] == 'h':
            hours = int(val[0:val.find('h')])
            val = s[i+1:]
        elif not minutes and s[i] == 'm':
            minutes = int(val[0:val.find('m')])
            break
    return (days * 24 * 60 * 60) + (hours * 60 * 60) + (minutes * 60)

def shorten_url(url):
    try:
        return '{}/.../{}'.format(url[:url.find('/', url.index('//')+2)], url[url.rindex('/')+1:])
    except:
        return url

def Fmt(s, obj, **kwargs):
    text = s.format_map(DictFmt(obj, **kwargs))
    if DEMOJIZE:
        import emoji
        text = emoji.demojize(text)
    return text

class DictFmt:
    def __init__(self, d, **kwargs):
        self.d = d
        self.args = kwargs

    def map_flag(self, flag):
        if flag: return '*'
        else: return ''

    def __getitem__(self, item):
        if item in self.d:
            val = self.d.__getitem__(item)
            if isinstance(val, datetime): return format_date(val)
            elif item == 'visibility': return map_visibility(val)
            elif item in ['favourited', 'reblogged']: return self.map_flag(val)
            else: return val
        elif item in self.args:
            return self.args.get(item)
        elif item == 'application':
            return {'name': 'none'}
        else:
            return '!!{}!!'.format(item)

