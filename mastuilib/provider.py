import logging
import threading
import urwid
from .widget.dialog import *
from .widget.toot import *

class ContentProvider:
    def __init__(self):
        self._ticks = 0
        self.content = []

    def get_title(self):
        return 'Untitled'

    def match(self, provider_id, api=None):
        return False

    def more(self):
        return False

    def reload(self):
        pass

    def redraw_selected(self):
        pass

    def tick(self, freq=None):
        if freq:
            self._ticks += 1
            if self._ticks >= freq:
                self._ticks = 0
                return True
        return False

    def keypress(self, size, key):
        return False

class TootProvider(ContentProvider):
    def __init__(self, api, items):
        super().__init__()
        self.api = api
        self.content = urwid.SimpleListWalker([urwid.AttrMap(TootBase.create(self, item), None, 'toot focus') for item in items])
        self.items = items 

    def append(self, items):
        if len(items) == 0:
            return False
        self.content.extend([urwid.AttrMap(TootBase.create(self, item), None, 'reveal focus') for item in items])
        self.items.extend(items)
        return True

    def prepend(self, items):
        if len(items) == 0:
            return False
        self.content[:0] = [urwid.AttrMap(TootBase.create(self, item), None, 'reveal focus') for item in items]
        self.items[:0] = items
        return True

    def clear(self):
        self.content.clear()
        self.items.clear()

    def match(self, provider_id, api=None):
        if self.timeline: return self.timeline == provider_id and api and self.api == api
        elif self.toot:   return self.toot['id'] == provider_id
        else:             return False

    def selected_toot(self):
        foc = self.content.get_focus()
        if foc: return foc[0].original_widget.get_toot()
        return None

    def selected_widget(self):
        foc = self.content.get_focus()
        if foc: return foc[0].original_widget
        return None

    def select_item(self, toot, opt):
        if opt == 0:
            card = toot.get('card')
            if card:
                self.api.app.open_links(link=(card['url'], card['type']))
            return

        opt = opt-1
        if toot['media_attachments'] and opt < len(toot['media_attachments']):
            file = toot['media_attachments'][opt]
            self.api.app.open_links(link=(file['url'], file['type']))
        elif toot['poll'] and opt < len(toot['media_attachments']):
            if opt < len(toot['poll']['options']):
                self.api().poll_vote(toot['poll']['id'], opt)
                self.api.app.update_status('Vote registered.')

    def select_items(self, toot):
        if toot['media_attachments']:
            self.api.app.open_links(links=[(file['url'], file['type']) for file in toot['media_attachments']])

    def redraw_selected(self):
        foc = self.content.get_focus()
        if foc: return foc[0].original_widget.redraw()
        return None

    def toggle_collapse(self):
        foc = self.content.get_focus()
        if foc: return foc[0].original_widget.toggle_collapse()
        return None

    def keypress(self, size, key):
        toot = self.selected_toot()
        cmd = self.api.app.keymap.map(key)
        if cmd == 'open_context':
            self.api.app.open_context_column(toot)
        elif cmd == 'fav_toot':
            def do_fav(args):
                self,toot,onoff = args
                self.api.toggle_favorite(toot, onoff)
                toot['favourited'] = onoff
                toot['favourites_count'] += (1 if onoff else -1)
            onoff = not toot['favourited']
            if self.api.cfg().getboolean('mastui', 'confirm_favorite', fallback=True):
                dlg = ConfirmDialog(self.api.app, self.api.app.lang.FAVORITE_P[int(onoff)].format_map(toot), do_fav, yes_arg=(self, toot, onoff)).show()
            else:
                self.do_fav((self, toot, onoff))
            self.redraw_selected()
        elif cmd == 'boost_toot':
            def do_boost(args):
                self,toot,onoff = args
                self.api.toggle_boost(toot, onoff)
                toot['reblogged'] = onoff
                toot['reblogs_count'] += (1 if onoff else -1)

            onoff =  not toot['reblogged']
            if self.api.cfg().getboolean('mastui', 'confirm_boost', fallback=True):
                dlg = ConfirmDialog(self.api.app, self.api.app.lang.BOOST_P[int(onoff)].format_map(toot), do_boost, yes_arg=(self, toot, onoff)).show()
            else:
                self.do_boost((self, toot, onoff))
            self.redraw_selected()
        elif cmd == 'bookmark_toot':
            onoff =  not toot['bookmarked']
            self.api.toggle_bookmark(toot, onoff)
            toot['bookmarked'] = onoff
            self.redraw_selected()
        elif cmd == 'select_*':
            self.select_items(toot)
        elif cmd and cmd.startswith('select_'):
            opt = cmd.split('_')[1]
            if opt == 'card': opt = 0
            self.select_item(toot, int(opt))
        elif cmd == 'open_raw':
            self.api.app.open_raw_column(toot)
        elif cmd == 'reload':
            self.reload()
        elif cmd == 'update':
            self.newer()
        elif cmd == 'reply':
            TootDialog(self.api, reply_to=toot).show()
        elif cmd == 'delete_toot':
            dlg = ConfirmDialog(self.api.app, self.api.app.lang.DELETE_P[1].format_map(toot), self.api.delete_toot, yes_arg=toot).show()
        elif cmd == 'follow_account':
            dlg = ConfirmDialog(self.api.app, self.api.app.lang.FOLLOW_P[1].format_map(toot['account']), self.api.follow_user, yes_arg=toot['account']).show()
        elif cmd == 'block_account':
            dlg = ConfirmDialog(self.api.app, self.api.app.lang.BLOCK_P[1].format_map(toot['account']), self.api.block_user, yes_arg=toot['account'], palette='confirm_urgent').show()
        elif cmd == 'mute_account':
            dlg = ConfirmDialog(self.api.app, self.api.app.lang.MUTE_P[1].format_map(toot['account']), self.api.mute_user, yes_arg=toot['account'], palette='confirm_caution').show()
        elif cmd == 'open_account':
            self.api.app.open_account_column(toot)
        elif cmd == 'toggle_collapse':
            self.toggle_collapse()
        else:
            return False
        return True

class TimelineProvider(TootProvider):
    def __init__(self, api, timeline, args=None):
        self.api = api
        self.timeline = timeline
        self.args = args
        toots = []
        func = lambda: self.api.app.loop.set_alarm_in(0,
                    lambda loop, data: self.append(data),
                    self.do_timeline())
        self.api.do_async(func)
        super().__init__(api, toots)

    def do_timeline(self, since_id=None, max_id=None):
        if self.timeline == 'hashtag':
            toots = self.api.timeline_hashtag(self.args, local=False, max_id=max_id, min_id=None, since_id=since_id, limit=self.api.cfg().getint('mastui', 'fetch_limit', fallback=None))
        else:
            toots = self.api.timeline(timeline=self.timeline, max_id=max_id, min_id=None, since_id=since_id, limit=self.api.cfg().getint('mastui', 'fetch_limit', fallback=None))
        return toots

    def get_title(self):
        if self.timeline == 'home': return 'Home [{}]'.format(self.api.alias)
        elif self.timeline == 'local': return 'Local [{}]'.format(self.api.alias)
        elif self.timeline == 'public': return 'Federated [{}]'.format(self.api.alias)
        elif self.timeline == 'hashtag': return 'Hashtag: #{} [{}]'.format(self.args, self.api.alias)
        else: return '{} [{}]'.format(self.timeline, self.api.alias)

    def more(self):
        max_id = None
        if len(self.items) > 0: max_id = self.items[-1]['id']
        func = lambda: self.api.app.loop.set_alarm_in(0,
                    lambda loop, data: self.append(data),
                    self.do_timeline(max_id=max_id))
        self.api.do_async(func)

    def newer(self):
        since_id = None
        if len(self.items) > 0: since_id = self.items[0]['id']
        func = lambda: self.api.app.loop.set_alarm_in(0,
                    lambda loop, data: self.prepend(data),
                    self.do_timeline(since_id=since_id))
        self.api.do_async(func)

    def tick(self):
        if super().tick(self.api.cfg().getint('mastui', 'poll_interval', fallback=None)):
            self.newer()

    def reload(self):
        self.clear()
        self.more()

class ContextProvider(TootProvider):
    def __init__(self, api, toot):
        self.api = api
        self.toot = toot
        toots = self.get_context(self.toot)
        super().__init__(api, toots)

    def get_context(self, toot):
        context = self.api().status_context(toot['id'])
        toots = context['ancestors']
        toots.append(toot)
        if 'descendants' in context:
            toots.extend(context['descendants'])
        return toots

    def reload(self):
        self.clear()
        self.append(self.get_context(self.toot))

    def match(self, provider_id, api=None):
        return self.toot['id'] == provider_id

    def get_title(self):
        return '{} [{}]'.format(str(self.toot['id']), self.api.alias)

class RawProvider(ContentProvider):
    def __init__(self, api, obj):
        super().__init__()
        self.api = api
        self.obj = obj

        import pprint
        text = pprint.pformat(obj, indent=4)
        self.text = urwid.Text(text)
        self.content = urwid.SimpleListWalker([self.text])

    def match(self, obj, api=None):
        return self.obj['id'] == obj['id']

    def get_title(self):
        return 'Raw {} [{}]'.format(str(self.obj['id']), self.api.alias)

class LogProvider(ContentProvider):
    def __init__(self, logbuf):
        super().__init__()
        self.logbuf = logbuf
        self.content = urwid.SimpleListWalker([urwid.AttrMap(urwid.Text(line[1]), 'log_{}'.format(line[0])) for line in self.logbuf.lines()])

    def tick(self):
        self.content[:] = [urwid.AttrMap(urwid.Text(line[1]), 'log_{}'.format(line[0])) for line in self.logbuf.lines()]

    def get_title(self):
        return 'Log'

class NotificationsProvider(TootProvider):
    def __init__(self, api):
        self.timeline = 'Notifications'
        self.api = api
        notifications = self.api.notifications(limit=self.api.cfg().getint('mastui', 'fetch_limit', fallback=None))
        super().__init__(api, notifications)

    def get_title(self):
        return 'Notifications [{}]'.format(self.api.alias)

    def match(self, provider_id, api=None):
        return self.get_title() == provider_id and self.api.alias == api.alias

    def selected_toot(self):
        foc = self.content.get_focus()
        if foc and 'status' in foc[0].original_widget.notification: return foc[0].original_widget.notification['status']
        return None

    def tick(self):
        if super().tick(self.api.cfg().getint('mastui', 'poll_interval', fallback=None)):
            self.newer()

    def more(self):
        max_id = None
        if len(self.items) > 0: max_id = self.items[-1]['id']
        func = lambda: self.api.app.loop.set_alarm_in(0,
                    lambda loop, data: self.append(data),
                    self.api.notifications(max_id=max_id, limit=self.api.cfg().getint('mastui', 'fetch_limit', fallback=None)))
        self.api.do_async(func)

    def newer(self):
        since_id = None
        if len(self.items) > 0: since_id = self.items[0]['id']
        func = lambda: self.api.app.loop.set_alarm_in(0,
                    lambda loop, data: self.prepend(data),
                    self.api.notifications(since_id=since_id, limit=self.api.cfg().getint('mastui', 'fetch_limit', fallback=None)))
        self.api.do_async(func)

class BookmarksProvider(TootProvider):
    def __init__(self, api):
        self.timeline = 'Bookmarks'
        self.api = api
        toots = self.api.bookmarks()
        super().__init__(api, toots)

    def get_title(self):
        return 'Bookmarks [{}]'.format(self.api.alias)

    def match(self, provider_id, api=None):
        return self.get_title() == provider_id and self.api.alias == api.alias

    def reload(self):
        self.clear()
        self.append(self.api.bookmarks())

class AccountProvider(TootProvider):
    def __init__(self, api, account_id):
        self.api = api
        self.account = self.api.account(account_id)
        toots = [] #self.api.bookmarks()
        super().__init__(api, toots)

    def get_title(self):
        return util.Fmt('Account #{id} [{O_api_alias}]', self.account, O_api_alias=self.api.alias)

